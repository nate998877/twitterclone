from django.db import models
from django.utils import timezone
from twitterclone.twitteruser.models import TwitterUser

class twot(models.Model):
    user = models.ForeignKey(TwitterUser, on_delete=models.CASCADE)
    time_submit = models.DateTimeField(auto_now_add=True)
    content = models.CharField(max_length=300)