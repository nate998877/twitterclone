from django.shortcuts import render, HttpResponseRedirect, reverse
from .forms import AddTwitterUser


def add_twitterUser(request):
    if request.method == "POST":
        form = AddTwitterUser(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            if user:= (
                    authenticate(
                        username=data["username"],
                        password=data["password"]
                    )
                ):
                login(request, user)
                return HttpResponseRedirect(
                    request.GET.get('next', reverse('homepage'))
                )
            else:
                return HttpResponseRedirect(
                    request.GET.get('next', reverse('add_user'))
                )

    form = AddTwitterUser()
    return render(request, 'generic_form.html', {'form': form})
