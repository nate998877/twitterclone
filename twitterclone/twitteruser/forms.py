from django import forms
from .models import TwitterUser

class AddTwitterUser(forms.Form):
    username = forms.CharField()
    password = forms.PasswordInput()
    email    = forms.EmailInput()