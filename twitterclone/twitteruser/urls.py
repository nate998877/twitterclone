from django.contrib import admin
from django.urls import path
from django.contrib.auth.admin import UserAdmin
from .models import TwitterUser
from .views import *

admin.site.register(TwitterUser)

urlpatterns = [
  path('add_user/', add_twitterUser, name='add_user'),
]